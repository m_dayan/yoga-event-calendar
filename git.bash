CURRENT_TIME=$(date +"%Y.%m.%d-%H.%M")
PARENDIR="$(dirname "$PWD")"

sudo cp -r $PWD/ /var/www/html/wordpress/wp-content/plugins/
echo 'kopiert';

if [ -z $2 ];then
    COMMENT=''
else
    COMMENT=$2
fi
if [ "$1" == "git" ];then
    echo "git called"
    git add *
    git commit -m "dev $CURRENT_TIME $COMMENT"
    git push origin dev
    echo "git completed"
fi
sudo chown www-data:www-data  -R /var/www/html/wordpress/wp-content/plugins/ # Let Apache be owner
sudo find /var/www/html/wordpress/wp-content/plugins/ -type d -exec chmod 755 {} \;  # Change directory permissions rwxr-xr-x
sudo find /var/www/html/wordpress/wp-content/plugins/ -type f -exec chmod 644 {} \;
echo 'permissions set'